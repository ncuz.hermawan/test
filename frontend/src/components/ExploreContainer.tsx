import React from 'react';
import './ExploreContainer.css';
import * as AntD from 'antd';
import * as PropTypes from "prop-types";
import {APIroutes} from '../routes/APIRoute';

interface ContainerProps {
  name: string;
}
const FormItem = AntD.Form.Item;
const EditableContext:any = React.createContext({});
const EditableRow = ({ form, index, ...props }:any) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);
const EditableFormRow = AntD.Form.create()(EditableRow);

class EditableCell extends React.Component<any, any> {
  public form:any;
  public input:any;


   constructor(props:any){
            super(props);
             this.state = {
                      editing: false
                    };
           //console.log('sssss',this.props)

    }
  componentDidUpdate(){
         // console.log('xxxx',this.props)


        }


  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  };

  save = (e:any) => {
    const { record, handleSave,handleForm }:any = this.props;
    this.form.validateFields((error:any, values:any) => {
      if (error && error[e.currentTarget.id]) {
        return;
      }
      this.toggleEdit();
      //handleForm(this.form);
      handleSave({ ...record, ...values });
    });
  };

  validation=(rule:any,value:any,callback:any)=>{
        //const {  handleForm }:any = this.props;
        //handleForm(this.form);
           if(value!==''){
                // console.log("validation",rule,value,value==='')
                 callback()
                 return
           }
           callback();
  }

  renderCell = (form:any) => {
    this.form = form;

    const { children, dataIndex, record, title,handleForm } = this.props;
    const { editing } = this.state;
    handleForm(this.form);
    // console.log('renderCell',editing)
    return editing ? (
      <AntD.Form.Item style={{ margin: 0 }}>
        {form.getFieldDecorator(dataIndex, {
          rules: [
            {
              required: true,
              message: `${title} is required.`,
            }, {
                validator: this.validation,
             }
          ],
          initialValue: record[dataIndex],
        })(<AntD.Input placeholder="IP Address" ref={node => (this.input = node)} onPressEnter={this.save} onBlur={this.save} />)}
      </AntD.Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{ paddingRight: 24 }}
        onClick={this.toggleEdit}
      >
        {children}
      </div>
    );
  };

  render() {
    const {
      editable,
      dataIndex,
      title,
      record,
      index,
      handleSave,
      handleForm,
      children,
      ...restProps
    } = this.props;

    return (
      <td {...restProps}>
        {editable ? (
          <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
        ) : (
          children
        )}
      </td>
    );
  }
}

const data = [
  {
    key: '0',
    ip: '27.0.0.1',
    active:false
  },
  {
    key: '1',
    ip: '10.59.67.42',
    active:true
  },
  {
    key: '2',
    ip: '10.59.99.26',
    active:true
  }
];
const rowSelection = {

};

class ExploreContainer extends React.Component<any, any> {
    private formEdit:any;
    public form:any;
    public  queue:any=false;
    public ds:any=null;
    public isLoading=false
    public loadingcount:any=0;
    public columns= [
  {
    title: 'REQUEST BOOKING ID',
    dataIndex: 'idrequestbooking',
    key: 'idrequestbooking',
    editable: false,
    render: (text:any) => <a>{text}</a>,
  },
  {
      title: 'PLATFORM ID',
      dataIndex: 'id_platform',
      key: 'id_platform',
      editable: true,
      render: (text:any) => <a>{text}</a>,
    },
  {
      title: 'DOC TYPE',
      dataIndex: 'doc_type',
      key: 'doc_type',
      editable: true,
      render: (text:any) => <a>{text}</a>,
    },
  {
      title: 'PLATFORM NAME',
      dataIndex: 'name_platform',
      key: 'name_platform',
      editable: true,
      render: (text:any) => <a>{text}</a>,
    },
   {
       title: 'TERM OF PAYMENT',
       dataIndex: 'term_of_payment',
       key: 'term_of_payment',
       editable: true,
       render: (text:any) => <a>{text}</a>,
     },
      {
        title: '',
        dataIndex: 'operation',
        render: (text:any, record:any) =>
          this.state.data.length >= 1 ? (
            <AntD.Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <AntD.Button   type="danger"
                                   style={{  color:'white',fontWeight:'bold' }} >
                                      Delete
                                   </AntD.Button>
            </AntD.Popconfirm>
          ) : null,
      },
    ];
    constructor(props:any){
            super(props);
                       this.state = {
                            data:data,
                            toggle:true,
                            toggleLabel:"Turn Off All",
                       }
    }
    handleAdd = () => {
        console.log('handleAdd');
        const { data } = this.state;
            let count=data.length;

             const newData = {
                  key: data.length,
                  idrequestbooking: '',
                  id_platform: '',
                  name_platform: '',
                  doc_type: '',
                  term_of_payment:'',
                };
                this.setState({
                  data: [...data, newData]
                });


      };
      handleresponse(response:any,container:any,ds:any){
                    if(typeof(response.default)==='undefined')
                       ds.ds=response;
                       else
                       ds.ds= response.default;

                     console.log("handleresponse Type 1",ds.ds)
                    container.queue=false;
                    let list:any=[];
                    let index=0;
                       ds.ds.forEach((option:any) => {
                        //  console.log("option",option)
                           let record={'key':index,'idrequestbooking':option['idrequestbooking'],'id_platform':option['id_platform']
                           ,'name_platform':option['nama_platform'],'doc_type':option['doc_type']
                           ,'term_of_payment':option['term_of_payment']}
                          // console.log("record",record)
                           list.push(record);
                          index++;
                       })
                       //console.log("list",list)
                       this.ds=list;
                       this.handleloading(false);
                       this.setState({
                                                   data:[...list]
                                             });

              }
   handleloading(isload:any){

             var count=this.loadingcount
             if(isload){
               console.log("handleloading WWW",count,this.state.isLoading,this.state.loadingcount)
               this.isLoading=true
               this.loadingcount=(count++)+1
             }else{
                console.log("handleloading XXXX",count,this.state.isLoading,this.state.loadingcount)
                 count=count-1
               this.isLoading=false
               this.loadingcount=count
             }

             if(count===0){
                // console.log("handleloading ssss",this.isLoading)
                  this.isLoading=false
             }
           }
    handleerror(error:any){
           // console.log('ERROR',error);
            //     this.ds='error';
                  this.isLoading=false;
                // this.handleloading(false);
             }
    handleDelete = (key:any) => {
          const data = [...this.state.data];
          this.setState({ data: data.filter(item => item.key !== key) });
        };
    handleSave = (row:any) => {
        console.log('handleSave',row)
       const newData = [...this.state.data];
       const index = newData.findIndex(item => row.key === item.key);
       const item = newData[index];
       newData.splice(index, 1, {
         ...item,
         ...row,
       });
       this.setState({ data: newData });
     };
   handleForm=(form:any)=>{
                       this.form=form;
                      //console.log("handle Form ZZZZ ",form);
   }
    loadAllClassificationDataSource=(e:any)=>{

             if(this.ds===null){
                   if(this.queue) return;
                   //console.log('loadAllClassificationDataSource',e,this.queue,this.ds)
                   //console.log('loadOfficeByNameDataSource QQQQQ',e,this.state.forms.classnameds,isOnline)
                   this.queue=true;
                   this.isLoading=true;
                    //this.handleloading(true);
                       APIroutes.LogService.LoadCollection( APIroutes.LogService.getLogList(),e,this,this)


             }
                  // LoadCollection( getAllOffice(),e,this.state.officename)
    }
    onSubmit=()=>{
        this.form.validateFields((error:any, values:any) => {
                // console.log("onSubmit validate",values);
                  if (error  ) {
                         AntD.notification["error"]({
                                  message: 'Control Panel - Task Scheduler',
                                  description: error.ip.errors[0].message+" Process Submit Abort.",
                                  duration: 5,
                                  style: {background:  this.context.theme.acrylicTexture80.background,
                                  fontWeight:'bold'}
                                });
                      return;
                   }
                let data:any=[];
                let index=0;
                let message=''
                this.state.data.forEach((option:any) => {

                 let record={'idrequestbooking':option.idrequestbooking,
                              'name_platform':option['name_platform'],
                              'id_platform':option['id_platform'],'doc_type':option['doc_type'],
                                              'term_of_payment':option['term_of_payment']}

                    console.log("onSubmit",option);
                   data.push(record);
                    index++;
                 })


                    // console.log("onSubmit data",isBlankIP,data,this.form);
                    APIroutes.LogService.setLogList(data)
                        .then((response:any)  => {
                            let ip=JSON.stringify(response);
                            //console.log("setServerList:"+JSON.parse(JSON.parse(JSON.stringify(ip))),
                            //response[0])
                            AntD.notification["success"]({
                              message: 'Test',
                              description:  "Log List Updated, Process Submit Done.",
                              duration: 5,
                              style: {color: 'black !important',
                              fontWeight:'bold'}
                            });
                     }).catch((error:any)=>{
                         AntD.notification["error"]({
                              message: 'Test',
                              description:  "Service Disconnect, Process Submit Abort.",
                              duration: 5,
                              style: {color:'black !important',
                              fontWeight:'bold'}
                            });
                      })


         })
      }
    render(){
        const components = {
              body: {
                row: EditableFormRow,
                cell: EditableCell,
              },
            };
    this.formEdit=components;
     //console.log('xxxx',this.state.data)
     const columns = this.columns.map(col => {
       //   console.log('xxxx',col,col.editable)
       if (!col.editable) {

         return col;
       }
       return {
         ...col,
         onCell: (record:any) => ({
           record,
           editable: col.editable,
           dataIndex: col.dataIndex,
           title: col.title,
           handleSave: this.handleSave,
           handleForm:this.handleForm
         }),
       };
      });
        this.loadAllClassificationDataSource(this);
        return (

            <div className="tableServer">
                 <div >
                     <AntD.Button onClick={this.handleAdd} type="primary"
                     style={{  color:'white',fontWeight:'bold' }} >
                        Add Log
                     </AntD.Button>
                 </div>


                     <AntD.Button onClick={this.onSubmit} type="primary"
                     style={{  color:'white',fontWeight:'bold' }} >
                        Submit
                     </AntD.Button>
                    <AntD.Table
                        components={components}
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={this.state.data} />
            </div>
        )
    }
}

export default ExploreContainer;
