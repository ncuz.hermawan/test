import {utilities} from '../../utilities';
import {ACCESS_TOKEN, PROXY_URL} from '../../config';


const proxyUrl = PROXY_URL;
const targetUrl ="/api";

export const API_BASE_URL=proxyUrl+targetUrl;


export function getLogList() {

    const option={
    		url: proxyUrl+targetUrl+ "/get-log",
            method: 'GET'
       }
    return utilities.APIHelper.setRequest(option);
}

export function setLogList(bodyRequest) {
	// console.log(" setServerList : ",JSON.stringify(bodyRequest),bodyRequest)
	const option={
			 url: proxyUrl+targetUrl+ "/post-log",
		     method: 'POST',
		     body: JSON.stringify(bodyRequest)
        }
	return utilities.APIHelper.setRequest(option);
}

export function LoadCollection(handlefunction,init,container,ds){
    init.handleloading(true);
    handlefunction.then(response => {
      init.handleresponse(response,container,ds)

    }).catch(error => {
       init.handleerror(error)
    })

}

