

export const request = (options,offline) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })
    


    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);
    return fetch(options.url, options)
    .then(response =>  
        response.json().then(json => {
        	//  console.log("API RESPONSE : ",json,options.url)
            if(!response.ok) {
            	return Promise.reject(json); 
            }
             // console.log("API RESPONSE : ",json)
            return json;
        }) 
    ).catch(error => {
        return Promise.reject(error);
    });
}; 

export const setRequest=(options,offline)=>{
	return request(options,offline);
}

