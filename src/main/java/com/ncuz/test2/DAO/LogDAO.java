package com.ncuz.test2.DAO;

import com.ncuz.test2.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface LogDAO extends JpaRepository<Log, Integer> {
    @Query("SELECT DISTINCT u FROM Log u WHERE u.idRequestBooking = :id")
    Log findLogByID(@Param("id") String id);
    @Query("SELECT DISTINCT u FROM Log u ")
    List<Log> findLogAll();
}
