package com.ncuz.test2.controller;


import com.ncuz.test2.service.LogService;
import com.ncuz.test2.utilities.JSONService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@Api(value="onlinestore", description="Operations pertaining to  Log Service")
public class LogController {
    @Autowired
    LogService logService;
    @Autowired
    JSONService jsonService;

    @ApiOperation(value = "Post Log", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @PostMapping("/post-log")
    public ResponseEntity<JSONObject> registerCustomer(@RequestBody String body){
        System.out.println("Register Log :"+body);
        JSONArray ds=jsonService.convertBodytoJSONArray(body);
//
        JSONObject result=logService.createLog(ds);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    };

    @ApiOperation(value = "Put Log", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @PutMapping("/put-log")
    public ResponseEntity<JSONObject> editCustomer(@RequestBody String body){
        System.out.println("Edit Log");
        JSONObject ds=jsonService.convertBodytoJSONObject(body);
        JSONObject result=logService.updateLog(ds);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    };

    @ApiOperation(value = "Get All Log", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/get-log")
    public ResponseEntity<JSONArray> getAllLog(){
        System.out.println("Get Log");

        JSONArray result=logService.getAllLog();
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    };

    @ApiOperation(value = "Get Log By ID", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @GetMapping("/get-log/{id}")
    public ResponseEntity<JSONObject> getLog(@PathVariable String id){
        System.out.println("Get Log :"+id );

        JSONObject result=logService.getLog(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    };

    @ApiOperation(value = "Delete Log by ID", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @DeleteMapping("/delete-log/{id}")
    public ResponseEntity<JSONObject> deleteLog(@PathVariable String id){
        System.out.println("Delete Log");

        JSONObject result=logService.deleteLog(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    };



}
