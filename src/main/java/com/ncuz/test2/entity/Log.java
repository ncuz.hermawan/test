package com.ncuz.test2.entity;

import javax.persistence.*;

@Entity
@Table(name="log_nle_api01")
public class Log {
    @Id
    @Column(name = "idrequestbooking",length=255,unique=true,nullable=false)
    private String idRequestBooking;

    @Column(name = "id_platform",length=20)
    private String idPlatform;

    @Column(name = "nama_platform",length=20)
    private String namaPlatform;

    @Column(name = "doc_type",length=20)
    private String docType;
    @Column(name = "term_of_payment",length=5)
    private String termOfPayment;

    public Log() {
    }

    public Log(String idRequestBooking, String idPlatform, String namaPlatform, String docType, String termOfPayment) {
        this.idRequestBooking = idRequestBooking;
        this.idPlatform = idPlatform;
        this.namaPlatform = namaPlatform;
        this.docType = docType;
        this.termOfPayment = termOfPayment;
    }

    public String getIdRequestBooking() {
        return idRequestBooking;
    }

    public void setIdRequestBooking(String idRequestBooking) {
        this.idRequestBooking = idRequestBooking;
    }

    public String getIdPlatform() {
        return idPlatform;
    }

    public void setIdPlatform(String idPlatform) {
        this.idPlatform = idPlatform;
    }

    public String getNamaPlatform() {
        return namaPlatform;
    }

    public void setNamaPlatform(String namaPlatform) {
        this.namaPlatform = namaPlatform;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getTermOfPayment() {
        return termOfPayment;
    }

    public void setTermOfPayment(String termOfPayment) {
        this.termOfPayment = termOfPayment;
    }
}
