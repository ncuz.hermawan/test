package com.ncuz.test2.service;

import com.ncuz.test2.DAO.LogDAO;
import com.ncuz.test2.entity.Log;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogService {
    @Autowired
    LogDAO logDAO;

    public JSONObject createLog(JSONArray data){
        System.out.println("createCustomer :"+data);
        JSONObject result=new JSONObject();
        for(int i=0;i<data.size();i++) {
            JSONObject record= (JSONObject) data.get(i);
            Log log = logDAO.findLogByID(record.get("idrequestbooking").toString());
            if (log == null) {
                log = new Log(
                        record.get("idrequestbooking").toString(),
                        record.get("id_platform").toString(),
                        record.get("name_platform").toString(),
                        record.get("doc_type").toString(),
                        record.get("term_of_payment").toString()
                );
                logDAO.save(log);
                result.put("result", "Create New Log Successfully");
            } else {
            System.out.println("Create Log Already Exist :"+record);
//            result.put("result","Create New Log Failed,Log "+log.getIdRequestBooking()+" Already Exist");
                log.setIdPlatform(record.get("id_platform").toString());
                log.setNamaPlatform(record.get("name_platform").toString());
                log.setDocType(record.get("doc_type").toString());
                log.setTermOfPayment(record.get("term_of_payment").toString());
                logDAO.save(log);
            }
        }

        return result;
    }

    public JSONObject updateLog(JSONObject data){
        System.out.println("updateLog :"+data+" | "+data.get("idrequestbooking").toString());
        JSONObject result=new JSONObject();
      Log log=logDAO.findLogByID(data.get("idrequestbooking").toString());
       if(log==null){
            result.put("result","Update Log Failed,Log "+log.getIdRequestBooking()+" Not Found");
        }else{
           log.setIdPlatform(data.get("id_platform").toString());
           log.setNamaPlatform(data.get("nama_platform").toString());
           log.setDocType(data.get("doc_type").toString());
           log.setTermOfPayment(data.get("term_of_payment").toString());
           logDAO.save(log);
            result.put("result","Update Log Successfully");
        }

        return result;
    }


    public JSONObject getLog(String id) {
        JSONObject result=new JSONObject();
        Log log=logDAO.findLogByID(id);
        if(log!=null){
            result.put("idrequestbooking",log.getIdRequestBooking());
            result.put("id_platform",log.getIdPlatform());
            result.put("nama_platform",log.getNamaPlatform());
            result.put("doc_type",log.getDocType());
            result.put("term_of_payment",log.getTermOfPayment());
        }
        return result;
    }

    public JSONArray getAllLog() {
        JSONArray result=new JSONArray();
        List<Log> log=logDAO.findLogAll();

        for(int i=0;i<log.size();i++){
            Log data=log.get(i);
            JSONObject result2=new JSONObject();
            result2.put("idrequestbooking",data.getIdRequestBooking());
            result2.put("id_platform",data.getIdPlatform());
            result2.put("nama_platform",data.getNamaPlatform());
            result2.put("doc_type",data.getDocType());
            result2.put("term_of_payment",data.getTermOfPayment());
//            System.out.println("getAllLog :"+result2);
            result.add(result2);
        }

        return result;
    }

    public JSONObject deleteLog(String id) {
        JSONObject result=new JSONObject();
        Log log=logDAO.findLogByID(id);
        if(log!=null){
            logDAO.delete(log);
            result.put("result","Delete Log Successfully");
        }

        return result;
    }
}
