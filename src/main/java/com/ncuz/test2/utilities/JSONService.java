package com.ncuz.test2.utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class JSONService {
    @PostConstruct
    private void post() {
        System.out.println("POST JSONService" );

    }
    public JSONObject convertBodytoJSONObject(String data){
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        try {
            json = (JSONObject) parser.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return json;
    }

    public JSONArray convertBodytoJSONArray(String data){
        JSONParser parser = new JSONParser();
        JSONArray json = null;
        try {
            json = (JSONArray) parser.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return json;
    }
}
